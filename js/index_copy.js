"use strict";

const DATA = [
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m1.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "8 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f1.png",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m2.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Тетяна",
    "last name": "Мороз",
    photo: "./img/trainers/trainer-f2.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
  },
  {
    "first name": "Сергій",
    "last name": "Коваленко",
    photo: "./img/trainers/trainer-m3.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
  },
  {
    "first name": "Олена",
    "last name": "Лисенко",
    photo: "./img/trainers/trainer-f3.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
  },
  {
    "first name": "Андрій",
    "last name": "Волков",
    photo: "./img/trainers/trainer-m4.jpg",
    specialization: "Бійцівський клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
  },
  {
    "first name": "Наталія",
    "last name": "Романенко",
    photo: "./img/trainers/trainer-f4.jpg",
    specialization: "Дитячий клуб",
    category: "спеціаліст",
    experience: "3 роки",
    description:
      "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
  },
  {
    "first name": "Віталій",
    "last name": "Козлов",
    photo: "./img/trainers/trainer-m5.jpg",
    specialization: "Тренажерний зал",
    category: "майстер",
    experience: "10 років",
    description:
      "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
  },
  {
    "first name": "Юлія",
    "last name": "Кравченко",
    photo: "./img/trainers/trainer-f5.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
  },
  {
    "first name": "Олег",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-m6.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "12 років",
    description:
      "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
  },
  {
    "first name": "Лідія",
    "last name": "Попова",
    photo: "./img/trainers/trainer-f6.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
  },
  {
    "first name": "Роман",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m7.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
  },
  {
    "first name": "Анастасія",
    "last name": "Гончарова",
    photo: "./img/trainers/trainer-f7.jpg",
    specialization: "Басейн",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
  },
  {
    "first name": "Валентин",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-m8.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
  },
  {
    "first name": "Лариса",
    "last name": "Петренко",
    photo: "./img/trainers/trainer-f8.jpg",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "7 років",
    description:
      "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
  },
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m9.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "11 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f9.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m10.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Наталія",
    "last name": "Бондаренко",
    photo: "./img/trainers/trainer-f10.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "8 років",
    description:
      "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
  },
  {
    "first name": "Андрій",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m11.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
  },
  {
    "first name": "Софія",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-f11.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "6 років",
    description:
      "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
  },
  {
    "first name": "Дмитро",
    "last name": "Ковальчук",
    photo: "./img/trainers/trainer-m12.png",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
  },
  {
    "first name": "Олена",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-f12.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "5 років",
    description:
      "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
  },
];
let copyData = [...DATA];


let containerCard = document.querySelector(".trainers-cards__container");
let modCardTemplate = document.querySelector("#modal-template").content;
let cardTampate = document.querySelector("#trainer-card").content;
// створює картки та модалку згідно переданої data
function CreatCard(dataArr) {
  dataArr.forEach((e) => {
    let clonCardTemplate = cardTampate.cloneNode(true);
    let newLi = document.createElement("li");
    newLi.classList.add("trainer");
    containerCard.append(newLi);
    let newImg = clonCardTemplate.querySelector(".trainer__img");
    newImg.src = e.photo;
    newLi.append(newImg);
    let newP = clonCardTemplate.querySelector(".trainer__name");
    newP.textContent = `${e["last name"]} ${e["first name"]}`;
    newLi.append(newP);
    let newBtn = clonCardTemplate.querySelector(".trainer__show-more");
    newBtn.textContent = "ПОКАЗАТИ";
    newLi.append(newBtn);

    newBtn.addEventListener("click", () => {
      let clonModCardTemplate = modCardTemplate.cloneNode(true);
      document.body.style.overflow = "hidden";
      clonModCardTemplate.querySelector(".modal__img").src = e.photo;
      clonModCardTemplate.querySelector(".modal__name").textContent = `${e["last name"]} ${e["first name"]}`;
      clonModCardTemplate.querySelector(".modal__point--category").textContent = `Категорія: ${e.category}`;
      clonModCardTemplate.querySelector(".modal__point--experience").textContent = `Досвід: ${e.experience}`;
      clonModCardTemplate.querySelector(".modal__point--specialization").textContent = `Напрям тренера: ${e.specialization}`;
      clonModCardTemplate.querySelector(".modal__text").textContent = e.description;
      clonModCardTemplate.querySelector(".modal__close").addEventListener("click", () => {
          document.querySelector(".modal").remove();
          document.body.style.overflow = "auto";
        });
      document.body.append(clonModCardTemplate);
    });
  });
}
CreatCard(DATA);

let getElsorting = document.querySelector(".sorting");
let getElsidebar = document.querySelector(".sidebar");
getElsorting.hidden = false;
getElsidebar.hidden = false;

// додав data-atribut для кнопок для перебору (спробував та залишив)
let getSortingBtn = document.querySelectorAll(".sorting__btn");
getSortingBtn.forEach((el) => {
  el.setAttribute("data-id", el.textContent.trim());
});

// перебірає кнопки актів та сортує картки 
let SortingClick = function (el) {
  if (el.target.type === "button") {
    el.target.classList.add("sorting__btn--active");

    for (let button of getSortingBtn) {
      if (button !== el.target) {
       button.classList.remove("sorting__btn--active");
      }
    }
  }

  switch (el.target.dataset.id) {
    case "ЗА замовчуванням": {
      containerCard.innerHTML = "";
      CreatCard(copyData);
      break;
    }
    case "ЗА ПРІЗВИЩЕМ": {
      DATA.sort((a, b) => a["last name"].localeCompare(b["last name"]));
      containerCard.innerHTML = "";
      CreatCard(DATA);
      break;
    }
    case "ЗА ДОСВІДОМ": {
      DATA.sort((a, b) => {
        let experienceA = parseInt(a.experience);
        let experienceB = parseInt(b.experience);
        return experienceB - experienceA;
      });
      containerCard.innerHTML = "";
      CreatCard(DATA);
      break;
    }
  }
  
}
//  прслуховувач кнопок для сортування карток 
getElsorting.addEventListener("click", (event) => {
  SortingClick(event)
});



// Sidefiltr____

let getFormInputs = document.querySelector(".sidebar__filters");
let getSubmitInput = document.querySelector(".filters__submit");


//  фільтрація слухаючого інпута (напрям та категорія). Значення занесено в масиви
let imputDirection = [];
let inputCategory = [];
let result ;
let getAllInputs = document.querySelectorAll("input"); 
getAllInputs.forEach((input) => {
  input.addEventListener("click", () => {
    if (input.type === "radio" && input.name === "direction") {
      imputDirection = []
      imputDirection.push(document.querySelector(`label[for="${input.id}"]`).textContent.trim().toLowerCase());
      }
    if (input.type === "radio" && input.name === "category") {
      inputCategory = []
      inputCategory.push(document.querySelector(`label[for="${input.id}"]`).textContent.trim().toLowerCase()); 
    }
  
    if (imputDirection.length > 0 && inputCategory.length === 0) {
      result = DATA.filter((el) => el.specialization.toLowerCase() === imputDirection[0]);
    } else if (imputDirection.length === 0 && inputCategory.length > 0) {
      result = DATA.filter((el) => el.category === inputCategory[0]);
    } else if (imputDirection.length > 0 && inputCategory.length > 0) {
      result = DATA.filter((el) => el.specialization.toLowerCase() === imputDirection[0] && el.category === inputCategory[0]);
    }
    
    if (imputDirection[0] === "всі" && inputCategory[0] === "всі"){
      result = DATA
    }else if (imputDirection[0] === "всі"){
   
      result = DATA.filter((el) =>  el.category === inputCategory[0]);
    }else if (inputCategory[0] === "всі"){
      if (imputDirection.length > 0 ){
      result = DATA.filter((el) =>  el.specialization.toLowerCase() === imputDirection[0]);
    }
    
    }
    console.log(result);
  });
});


//  виводить результат відфільтрованих карток при натисканні кнопки "Показать"
getSubmitInput.addEventListener("click", (e) => {
  e.preventDefault();
  containerCard.innerHTML = "";
  CreatCard(result);
})








// 111111111111
// window.addEventListener("click", (event) => {
//   console.log(event.target);
// });
// 11111111111111111